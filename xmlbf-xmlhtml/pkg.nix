{ mkDerivation, base, bytestring, html-entities, stdenv, tasty
, tasty-hunit, text, unordered-containers, xmlbf, xmlhtml
}:
mkDerivation {
  pname = "xmlbf-xmlhtml";
  version = "0.2.1";
  src = ./.;
  libraryHaskellDepends = [
    base bytestring html-entities text unordered-containers xmlbf
    xmlhtml
  ];
  testHaskellDepends = [ base tasty tasty-hunit xmlbf ];
  homepage = "https://gitlab.com/k0001/xmlbf";
  description = "xmlhtml backend support for the xmlbf library";
  license = stdenv.lib.licenses.asl20;
}
