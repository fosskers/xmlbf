{ pkgs }:

let
hs = pkgs.haskell.lib;
selective =
  { mkDerivation, base, containers, mtl, QuickCheck, stdenv, tasty
  , tasty-expected-failure, tasty-quickcheck, transformers
  }:
  mkDerivation {
    pname = "selective";
    version = "0.2";
    sha256 = "ba82041168d91347c73deae8fcce72506bccf1b730c738f977bd39b747bbe075";
    libraryHaskellDepends = [ base containers transformers ];
    testHaskellDepends = [
      base containers mtl QuickCheck tasty tasty-expected-failure
      tasty-quickcheck
    ];
    homepage = "https://github.com/snowleopard/selective";
    description = "Selective applicative functors";
    license = stdenv.lib.licenses.mit;
  };

in
# To be used as `packageSetConfig` for a Haskell pacakge set:
self: super:
{
  xmlbf = super.callPackage ./xmlbf/pkg.nix {};
  xmlbf-xeno = super.callPackage ./xmlbf-xeno/pkg.nix {};
  xmlbf-xmlhtml = super.callPackage ./xmlbf-xmlhtml/pkg.nix {};

  _shell = self.shellFor {
    withHoogle = false; # hoogle dependencies don't compile
    packages = p: [
      p.xmlbf
      p.xmlbf-xeno
      p.xmlbf-xmlhtml
    ];
  };

  # deps
  html-entities = hs.overrideCabal super.html-entities (_: {
    postUnpack = "rm -f html-entities-1.1.4.2/Setup.hs";
  });
  selective = super.callPackage selective {};
}
